<?php

require_once (__DIR__ . '/model.php');

$pastures = get_pastures();

foreach ($pastures as $id => $sheep) {
    if (count($sheep) > 1) {
        update_pasture($id);
    }
}

$pastures = get_pastures();

print json_encode([
    'status' => 'ok',
    'pastures' => $pastures,
]);
