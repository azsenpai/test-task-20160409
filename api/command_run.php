<?php

require_once (__DIR__ . '/model.php');

$command_text = get_value($_POST['command_text'], '');
$command = explode(' ', $command_text);

for ($i = 0, $n = count($command); $i < $n; $i ++) {
    $command[$i] = mb_strtolower($command[$i], 'UTF-8');
}

$status = 'ok';
$message = '';

switch (count($command)) {
    case 2:
        if ($command[0] != 'убить' || strncmp($command[1], 'овечка', 6)) {
            $status = 'error';
        } else {
            $sheep = (int)preg_replace('/\D/', '', $command[1]);

            if (!is_sheep_alive($sheep)) {
                $status = 'error';
                $message = sprintf('Овечка%d уже зарублена или ее вообще не было', $sheep);
            } else {
                kill_all([$sheep]);
            }
        }
        break;

    case 3:
        if (strncmp($command[0], 'овечка', 6) || $command[1] != 'переместить' || strncmp($command[2], 'загон', 5)) {
            $status = 'error';
        } else {
            $id = (int)preg_replace('/\D/', '', $command[2]);
            $sheep = (int)preg_replace('/\D/', '', $command[0]);

            if (!is_sheep_alive($sheep)) {
                $status = 'error';
                $message = sprintf('Овечка%d уже зарублена или ее вообще не было', $sheep);
            } elseif (!is_pasture_exists($id)) {
                $status = 'error';
                $message = sprintf('Загон%d не существует', $sheep);
            } else {
                move_all($id, [$sheep]);
            }
        }
        break;

    default:
        $status = 'error';
}

$pastures = [];

if ($status == 'ok') {
    $pastures = get_pastures();
}

print json_encode([
    'status' => $status,
    'pastures' => $pastures,
    'message' => $message,
]);
