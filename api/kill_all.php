<?php

require_once (__DIR__ . '/model.php');

$killed_amount = kill_all(get_value($_POST['sheep'], []));
update_killed_amount($killed_amount);

$pastures = get_pastures();

print json_encode([
    'status' => 'ok',
    'killed_amount' => $killed_amount,
    'pastures' => $pastures,
]);
