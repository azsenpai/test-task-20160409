<?php

$config = require_once(__DIR__ . '/../config/main.php');

function is_pasture_exists($id)
{
    global $config;

    $dbh = open_db();

    $statement = sprintf('SELECT * FROM pastures WHERE id = %d', $id);
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    return !empty($result) || (1 <= $id && $id <= $config['pasture-amount']);
}

function is_sheep_alive($sheep)
{
    $dbh = open_db();

    $statement = sprintf('SELECT * FROM pastures WHERE sheep = %d', $sheep);
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    return !empty($result);
}

function update_pasture($id)
{
    global $config;

    $dbh = open_db();

    $statement = 'INSERT INTO pastures VALUES ';
    $values = [];

    $last_sheep = get_last_sheep();
    $sheep = rand($config['rand']['min'], $config['rand']['max']);

    for ($j = 0; $j < $sheep; $j ++) {
        $values[] = sprintf('(%d, %d)', $id, $last_sheep);
        $last_sheep ++;
    }
    $statement .= implode(', ', $values);

    $result = $dbh->exec($statement);

    close_db($dbh);
    
    return $result;
}

function update_killed_amount($amount)
{
    $dbh = open_db();

    $statement = sprintf('UPDATE statistics SET value = value + %d WHERE `key` = "killed_amount"', $amount);
    $result = $dbh->exec($statement);

    close_db($dbh);

    return $result;
}

function move_all($id, $sheep)
{
    $sheep = (array)$sheep;
    $save_sheep = [];

    foreach ($sheep as $item) {
        $save_sheep[] = (int)$item;
    }

    if (empty($save_sheep)) {
        return 0;
    }

    $dbh = open_db();

    $statement = sprintf('DELETE FROM pastures WHERE sheep IN (%s)', implode(', ', $save_sheep));
    $result = $dbh->exec($statement);

    $statement = 'INSERT INTO pastures VALUES ';
    foreach ($save_sheep as $item) {
        $statement .= sprintf('(%d, %d)', $id, $item);
    }
    $result = $dbh->exec($statement);

    close_db($dbh);

    return $result;
}

function kill_all($sheep)
{
    $sheep = (array)$sheep;
    $save_sheep = [];

    foreach ($sheep as $item) {
        $save_sheep[] = (int)$item;
    }

    if (empty($save_sheep)) {
        return 0;
    }

    $dbh = open_db();

    $statement = sprintf('DELETE FROM pastures WHERE sheep IN (%s)', implode(', ', $save_sheep));
    $result = $dbh->exec($statement);

    close_db($dbh);

    return $result;
}

function get_killed_amount()
{
    $dbh = open_db();

    $statement = 'SELECT * FROM statistics WHERE `key` = "killed_amount"';
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    return empty($result) ? 0 : $result[0]['value'];
}

function get_last_sheep()
{
    $dbh = open_db();

    $statement = 'SELECT MAX(sheep) AS last_sheep FROM pastures';
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    return (empty($result) ? 0 : $result[0]['last_sheep']) + 1;
}

function get_pastures()
{
    global $config;

    $dbh = open_db();

    $statement = 'SELECT * FROM pastures';
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    $pastures = [];

    for ($i = 1; $i <= $config['pasture-amount']; $i ++) {
        $pastures[$i] = [];
    }

    for ($i = 0, $n = count($result); $i < $n; $i ++) {
        $pastures[$result[$i]['id']][] = $result[$i]['sheep'];
    }

    return $pastures;
}

function open_db()
{
    global $config;

    $dsn = sprintf('mysql:host=%s;dbname=%s', $config['db']['host'], $config['db']['dbname']);

    try {
        $dbh = new PDO($dsn, $config['db']['username'], $config['db']['password']);
    } catch (PDOException $e) {
        print 'Error!: ' . $e->getMessage();
        exit();
    }

    return $dbh;
}

function close_db($dbh)
{
    $dbh = null;
}

function get_value(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

function create_table_statistics()
{
    $statement = "
        CREATE TABLE IF NOT EXISTS `statistics` (
            `key` VARCHAR(50) NOT NULL,
            `value` INT(11) NOT NULL,
            PRIMARY KEY (`key`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;
    ";

    $dbh = open_db();

    $dbh->exec($statement);

    $statement = 'SELECT * FROM statistics WHERE `key` = "killed_amount"';
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    if (empty($result)) {
        $statement = 'INSERT INTO statistics VALUE("killed_amount", 0)';
        $dbh->exec($statement);
    }

    close_db($dbh);

    printf('%s is processed%s', __FUNCTION__, PHP_EOL);
}

function create_table_pastures()
{
    $statement = "
        CREATE TABLE IF NOT EXISTS `pastures` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `sheep` INT(11) NOT NULL DEFAULT '0',
            UNIQUE INDEX `id_sheep` (`id`, `sheep`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;
    ";

    $dbh = open_db();

    $dbh->exec($statement);

    $statement = 'SELECT * FROM pastures';
    $result = $dbh->query($statement)->fetchAll(PDO::FETCH_ASSOC);

    close_db($dbh);

    if (empty($result)) {
        global $config;

        for ($i = 1; $i <= $config['pasture-amount']; $i ++) {
            update_pasture($i);
        }
    }

    printf('%s is processed%s', __FUNCTION__, PHP_EOL);
}
