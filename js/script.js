jQuery(function($) {
    alertify.set('notifier', 'position', 'top-right');

    var $timeout = $('[name="timeout"]');

    if ($timeout.length) {
        var timeout = parseFloat($timeout.val());

        if (timeout > 0) {
            setInterval(update_data, timeout);
        }
    }

    $('.command_text').on('keypress', function(e) {
        if (e.keyCode == 13) {
            $('.command_run').trigger('click');
        }
    });

    $('.command_run').on('click', command_run);

    $('.update_data').on('click', update_data);

    $('.kill_all').on('click', function() {
        var sheep = [];

        $('.pasture option:selected').each(function() {
            sheep.push($(this).val());
        });

        kill_all(sheep);
    });

    function kill_all(sheep) {
        if (!sheep.length) {
            return;
        }

        $.ajax({
            type: 'post',
            url: '/api/kill_all.php',
            data: {
                sheep: sheep
            },
            dataType: 'json',
            success: function(response) {
                if (('status' in response) && response.status == 'ok' && response.killed_amount > 0) {
                    var message = '';

                    for (var i = 0; i < sheep.length; i ++) {
                        $('option[value="' + sheep[i] + '"]').remove();
                        message += 'Овечка' + sheep[i] + ', ';
                    }
                    message += (response.killed_amount > 1) ? 'были зарублены' : 'была зарублена';

                    alertify.error(message);

                    update_killed_amount(response.killed_amount);
                    update_pastures(response.pastures);
                }
            },
        });
    }

    function update_killed_amount(amount) {
        var $killed_amount = $('.killed-amount');

        if (!$killed_amount.length) {
            return;
        }

        $killed_amount.text(parseInt($killed_amount.text()) + parseInt(amount));
    }

    function command_run() {
        var $command_text = $('[name="command_text"]');
        var command_text = '';

        if (!$command_text.length) {
            return;
        }
        command_text = $.trim($command_text.val());

        if (!command_text.length) {
            return;
        }

        $.ajax({
            type: 'post',
            url: '/api/command_run.php',
            data: {
                command_text: command_text
            },
            dataType: 'json',
            success: function(response) {
                if ('status' in response) {
                    if (response.status == 'ok') {
                        update_pastures(response.pastures);
                        alertify.success(command_text + ', команда выполнена');
                    } else {
                        var message = response.message;

                        if (message.length > 0) {
                            alertify.error(message);
                        } else {
                            alertify.error(command_text + ', неизвестная команда');
                        }
                    }
                }
            }
        });
    }

    function update_data() {
        $.ajax({
            type: 'post',
            url: '/api/update_data.php',
            dataType: 'json',
            success: function(response) {
                if (('status' in response) && response.status == 'ok') {
                    update_pastures(response.pastures);
                    alertify.success('Данные были обновлены');
                }
            }
        });
    }

    function update_pastures(pastures) {
        for (pasture in pastures) {
            var $pasture = $('.pasture' + pasture);
            var $select = $pasture.find('select');

            $pasture.find('.amount').text(pastures[pasture].length);
            $select.html('');

            for (i in pastures[pasture]) {
                var sheep = pastures[pasture][i];
                $select.append('<option value="' + sheep + '">Овечка' + sheep + '</option>');
            }
        }
    }
});
