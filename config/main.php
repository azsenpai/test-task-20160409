<?php

return [
    'pasture-amount' => 4,
    'timeout' => 0,

    'db' => [
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'test_biznes',
    ],

    'rand' => [
        'min' => 1,
        'max' => 5,
    ],
];
