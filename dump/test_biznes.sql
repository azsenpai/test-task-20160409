-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.26 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных test_biznes
DROP DATABASE IF EXISTS `test_biznes`;
CREATE DATABASE IF NOT EXISTS `test_biznes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test_biznes`;


-- Дамп структуры для таблица test_biznes.pastures
DROP TABLE IF EXISTS `pastures`;
CREATE TABLE IF NOT EXISTS `pastures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sheep` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `id_sheep` (`id`,`sheep`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test_biznes.pastures: ~15 rows (приблизительно)
/*!40000 ALTER TABLE `pastures` DISABLE KEYS */;
INSERT INTO `pastures` (`id`, `sheep`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 4),
	(2, 5),
	(2, 6),
	(3, 7),
	(3, 8),
	(3, 9),
	(3, 10),
	(4, 11),
	(4, 12),
	(4, 13),
	(4, 14),
	(4, 15);
/*!40000 ALTER TABLE `pastures` ENABLE KEYS */;


-- Дамп структуры для таблица test_biznes.statistics
DROP TABLE IF EXISTS `statistics`;
CREATE TABLE IF NOT EXISTS `statistics` (
  `key` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test_biznes.statistics: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
INSERT INTO `statistics` (`key`, `value`) VALUES
	('killed_amount', 0);
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
