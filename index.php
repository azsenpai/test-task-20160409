<?php

require_once (__DIR__ . '/api/model.php');

$pastures = get_pastures();

?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <title>test-biznes</title>

        <link rel="icon" href="favicon.png">
        <link rel="stylesheet" href="css/alertify.css">
        <link rel="stylesheet" href="css/style.css">

        <script src="lib/jquery/jquery-1.11.2.min.js"></script>
        <script src="lib/alertify/alertify.min.js"></script>
        <script src="js/script.js"></script>

        <!--[if lt IE 9]>
            <script type="text/javascript" src="lib/html5shiv/html5shiv.min.js"></script>
        <![endif]-->        
    </head>
    <body>
        <div class="content-wrapper">
        	<div class="help-text">Для выбора нескольких овечек удерживайте клавишу Ctrl или Shift</div>
            <fieldset class="pastures">
                <legend>Фермочка для овечек</legend>
                <?php foreach ($pastures as $i => $pasture): ?>
                <div class="pasture<?= $i; ?> pasture">
                    <div class="title">Загон<?= $i; ?>: Число овечек <span class="amount"><?= count($pasture); ?></span></div>
                    <select multiple="multiple">
                        <?php foreach ($pasture as $sheep): ?>
                        <option value="<?= $sheep; ?>">Овечка<?= $sheep; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php endforeach; ?>
            </fieldset>
            <div class="controls">
                <button class="update_data">Обновить</button>
                <button class="kill_all">Зарубить овечек</button>
            </div>
            <div class="command">
                <input name="command_text" class="command_text" placeholder="Введите текст команды">
                <button class="command_run">Выполнить</button>
            </div>
            <div class="info">Степень Вашей маниакальности = <span class="killed-amount"><?= get_killed_amount(); ?></span> Единиц убитой живности</div>

            <input type="hidden" name="timeout" value="<?= $config['timeout'] * 1000; ?>">
        </div>
    </body>
</html>
